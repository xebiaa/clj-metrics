(ns clj-metrics.core
  (:use [clj-metrics.util]
        [clj-metrics.parser]
        [clj-metrics.reader]
        [clj-metrics.report])
  (:require [org.danlarkin.json :as json]))

(defn dump-json [units]
  (println (json/encode (merge-seq [:path (get-paths units)]
                                   [:ns (get-ns units)]
                                   [:loc (get-nr-of-lines units)]
                                   [:defns (get-nr-of-defns units)]
                                   [:cdefns (get-nr-of-commented-defns units)])
                        :indent 2)))

(defn -main [& [args]]
  (let [dir (if args args ".")
        units (parse (read-all-clj-files dir))]
    (dump-json units)))
