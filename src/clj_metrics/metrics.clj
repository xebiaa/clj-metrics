(ns clj-metrics.metrics
  (:use [clj-metrics.util] [clj-metrics.defn] [clj-metrics.reader])
  (:require [clojure.string :as str]
            [clojure.contrib.string :as str-utils]))

(defn nr-of-lines
  "Get number of lines for the given source"
  [unit]
  (count (str/split-lines (:src unit))))

(defn nr-of-blank-lines
  "Get number of blank lines for the given source"
  [source]
  (nr-of-lines (filter str-utils/blank? (str/split-lines source))))

(defn- nr-of-toplevel-symbols
  [ast symbol-name]
  (count (filter #(= (symbol symbol-name) (first %)) ast)))

; Parsing

(defn- get-all-defns
  "Get all functions defined in this ast"
  [unit]
  (filter is-defn? (:ast unit)))

(defn nr-of-defns
  "Count functions in file" 
  [unit]
  (count (get-all-defns unit)))

(defn nr-of-commented-defns
  "Count functions with comment string in file" 
  [unit]
  (count-if has-comment? (get-all-defns unit)))

