(ns clj-metrics.report
  (:use [clj-metrics.metrics]))

(defn get-paths [seq]
  "Returns all path names"
  (map :path seq))

(defn get-ns [seq]
  "Returns all namespaces"
  (map :ns seq))

(defn get-nr-of-lines [seq]
  (map nr-of-lines seq))

(defn get-nr-of-defns [seq]
  (map nr-of-defns seq))

(defn get-nr-of-commented-defns [seq]
  (map nr-of-commented-defns seq))

(defn- insert-unit [ns-tree unit]
  (conj ns-tree {:ns (:ns unit) :files [{:path (:path unit)}]}))

(defn build-ns-tree [seq]
  "Build tree based on namespace hierarchy"
  (if (empty? seq) []
      (insert-unit (build-ns-tree (rest seq)) (first seq))))

