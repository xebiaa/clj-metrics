(ns clj-metrics.parser
  (:use [clj-metrics.util] [clj-metrics.defn] [clj-metrics.reader])
  (:require [clojure.string :as str]
            [clojure.contrib.string :as str-utils]))

(defn create-ast
  "Create a kind of abstract syntax tree"
  [source]
  (read-string (str "(" source "\n)")))

(defn is-ns?
  "Return true if this decl defines a function, otherwise false"
  [decl]
  (= (symbol "ns") (first decl)))

(defn- parse-ns
  [ast]
  (second (first (filter is-ns? ast))))

(defn- parse-unit
  [unit]
  (let [ast (create-ast (:src unit))]
    (assoc unit :ast ast :ns (parse-ns ast))))

(defn parse
  "Parse source"
  [seq]
  (map parse-unit seq))