(ns clj-metrics.test.report
  (:use [clj-metrics.report]
        [clojure.test]))

(def single-unit [ {:src "one_src\nline2\nline3"
                 :ns "project"
                 :path "/src/project/package/one.clj"
                 :ast '()} ])

(def example [ {:src "one_src\nline2\nline3"
                 :ns "project.package.one"
                 :path "/src/project/package/one.clj"
                 :ast '()}
                {:src "two_src\nline2"
                 :ns "project.package.two"
                 :path "/src/project/package/two.clj"
                 :ast '()}
                {:src "three_src\nline2\nline3\nline4"
                 :ns "project.package.two.three"
                 :path "/src/project/package/two/three.clj"
                 :ast '()} ]) 

(def expected-tree [ { :ns "project"
                       :nodes [ { :ns "package"
                                  :nodes [ { :ns "one"
                                             :files [ { :path "one.clj" } ] }
                                           { :ns "two"
                                             :files [ { :path "two.clj" } ]
                                             :nodes [ { :ns "three"
                                                        :files [ { :path "three.clj" } ] } ] } ] } ] } ])

(def expected-single-unit [ { :ns "project"
                              :files [{:path "/src/project/package/one.clj"}]}])
                      
(deftest test-build-empty-tree
  (is
    (= [] (build-ns-tree []))))

(deftest test-build-single-unit-tree
  (is
    (= expected-single-unit (build-ns-tree single-unit))))

(deftest test-report
  (is
    (= expected-tree (build-ns-tree example))))

